package javari.writer;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Writer describes an object that can write a file to some path, given an object of type T.
 *
 * @author Raihan Ramadistra
 */
public interface Writer<T> {

    void write(T object, Path path) throws IOException;
}
