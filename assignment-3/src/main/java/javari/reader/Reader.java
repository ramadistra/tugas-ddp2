package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Reader describes an object that can read from some path and return an object of type T.
 *
 * @author Raihan Ramadistra
 */
public interface Reader<T> {

    T readFrom(Path path) throws IOException;
}
