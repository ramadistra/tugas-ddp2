package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javari.database.ParkDatabase;
import javari.database.ParkDatabaseImpl;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Condition;
import javari.model.Gender;
import javari.model.Status;
import javari.model.Type;

public class CsvDatabaseReader implements Reader<ParkDatabase> {

    private String categoriesConfig = "animals_categories.csv";
    private String attractionsConfig = "animals_attractions.csv";
    private String animalRecords = "animals_records.csv";
    private ValidatedReader<List<String[]>> categoriesReader = new NewCsvReader(new RegexMatcher(
        ".*(mammals,Explore the Mammals|aves,World of Aves|reptiles,Reptilian Kingdom)"),
        s -> s.split(",", 2)[1]);
    private ValidatedReader<List<String[]>> attractionsReader = new NewCsvReader(new RegexMatcher(
        ".*(Circles of Fires|Dancing Animals|Counting Masters|Passionate Coders)"),
        s -> s.split(",", 2)[1]);

    public ParkDatabase readFrom(Path path) throws IOException {
        ValidationData<List<String[]>> validCategories =
            categoriesReader.readFrom(path.resolve(categoriesConfig));
        ValidationData<List<String[]>> validAttractions =
            attractionsReader.readFrom(path.resolve(attractionsConfig));

        List<Type> types = parseCategoriesConfig(validCategories.getValidatedData());
        ValidatedReader<List<String[]>> animalsReader = new NewCsvReader(
            new RegexMatcher(createAnimalRegex(types)));
        ValidationData<List<String[]>> validAnimals =
            animalsReader.readFrom(path.resolve(animalRecords));

        System.out.println("\n... Loading... Success... System is populating data...\n"
            + "\n" + validationInfo("sections", validCategories)
            + "\n" + validationInfo("attractions", validAttractions)
            + "\n" + validationInfo("categories", validCategories)
            + "\n" + validationInfo("animal records", validAnimals));

        return new ParkDatabaseImpl(
            types,
            parseAttractionsConfig(validAttractions.getValidatedData()),
            parseAnimals(validAnimals.getValidatedData())
        );
    }

    private String createAnimalRegex(List<Type> types) {
        return "\\d+,("
            + types.stream().map(Type::toString).collect(Collectors.joining("|"))
            + "),[A-z]+,(fe)?male,\\d+(.\\d+)?,\\d+(.\\d+)?,(wild|tame|pregnant)?,(not )?healthy";
    }

    private String validationInfo(String identifier, ValidationData validationData) {
        return "Found " + validationData.getValidRecordsCount() + " valid " + identifier
            + " records and " + validationData.getInvalidRecordsCount() + " invalid " + identifier;
    }

    private Map<Type, List<Attraction>> parseAttractionsConfig(List<String[]> validatedLines) {
        Map<Type, List<Attraction>> attractionsByType = new HashMap<>();

        validatedLines.forEach(args -> attractionsByType
            .computeIfAbsent(Type.fromString(args[0]), why -> new ArrayList<>())
            .add(Attraction.parseAttractionName(args[1]))
        );

        return attractionsByType;
    }

    private List<Type> parseCategoriesConfig(List<String[]> validatedLines) {
        return validatedLines.stream()
            .map(args -> Type.fromString(args[0]))
            .distinct()
            .collect(Collectors.toList());
    }

    private List<Animal> parseAnimals(List<String[]> validatedLines) {
        return validatedLines.stream().map(args ->
            new Animal(Integer.parseInt(args[0]),
                Type.fromString(args[1]),
                args[2],
                Gender.parseGender(args[3]),
                Double.parseDouble(args[4]),
                Double.parseDouble(args[5]),
                Status.fromString(args[6]),
                Condition.parseCondition(args[7])
            )).collect(Collectors.toList());
    }
}
