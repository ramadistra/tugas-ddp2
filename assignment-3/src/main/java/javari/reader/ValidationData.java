package javari.reader;

public class ValidationData<T> {

    private T validatedData;
    private long validRecordsCount;
    private long invalidRecordsCount;

    public ValidationData(T validatedData, long validRecordsCount, long invalidRecordsCount) {
        this.validatedData = validatedData;
        this.validRecordsCount = validRecordsCount;
        this.invalidRecordsCount = invalidRecordsCount;
    }

    public T getValidatedData() {
        return validatedData;
    }

    public long getValidRecordsCount() {
        return validRecordsCount;
    }

    public long getInvalidRecordsCount() {
        return invalidRecordsCount;
    }
}
