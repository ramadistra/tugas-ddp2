package javari.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class NewCsvReader implements ValidatedReader<List<String[]>> {

    private UnaryOperator<String> selector;
    private Predicate<String> lineValidator;

    public NewCsvReader(Predicate<String> lineValidator, UnaryOperator<String> selector) {
        this.selector = selector;
        this.lineValidator = lineValidator;
    }

    public NewCsvReader(Predicate<String> lineValidator) {
        this(lineValidator, UnaryOperator.identity());
    }

    @Override
    public ValidationData<List<String[]>> readFrom(Path path) throws IOException {
        List<String> lines = Files.readAllLines(path);
        return new ValidationData<>(
            lines.stream().filter(lineValidator).map(this::toColumn).collect(Collectors.toList()),
            lines.stream().map(selector).distinct().filter(lineValidator).count(),
            lines.stream().map(selector).distinct().filter(lineValidator.negate()).count()
        );
    }

    private String[] toColumn(String line) {
        return line.split(",");
    }
}
