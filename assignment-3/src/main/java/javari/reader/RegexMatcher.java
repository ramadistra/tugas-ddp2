package javari.reader;

import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * RegexMatcher is a utility class to create a regex matcher that implements Predicate.
 *
 * @author Raihan Ramadistra
 */
public class RegexMatcher implements Predicate<String> {

    private Pattern pattern;

    public RegexMatcher(String regex) {
        pattern = Pattern.compile(regex);
    }

    public boolean test(String input) {
        return pattern.matcher(input).matches();
    }
}
