package javari.database;

import java.util.List;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Category;
import javari.model.Type;

/**
 * This interfaces describes the Park Database, with queries to be used in Javari Park.
 *
 * <p>The database contains Categories, Types, Animals and Attractions</p>
 *
 * @author Raihan Ramadistra Pratama - 1706027130
 */
public interface ParkDatabase {

    List<Category> getAllCategories();

    List<Type> getTypesByCategory(Category category);

    List<Attraction> getAttractionsByCategory(Type type);

    List<Animal> getShowableAnimalsByType(Type type);
}
