package javari.database;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Category;
import javari.model.Type;

public class ParkDatabaseImpl implements ParkDatabase {

    private List<Type> allowedTypes;
    private Map<Category, List<Type>> typesByCategory;
    private Map<Type, List<Attraction>> attractionsByType;
    private Map<Type, List<Animal>> animalsByType;

    public ParkDatabaseImpl(List<Type> allowedTypes,
        Map<Type, List<Attraction>> attractionsByType,
        List<Animal> animals) {
        this.allowedTypes = allowedTypes;
        this.typesByCategory = allowedTypes.stream()
            .collect(Collectors.groupingBy(Type::getCategory));
        this.attractionsByType = attractionsByType;
        this.animalsByType = animals.stream().collect(Collectors.groupingBy(Animal::getType));
    }

    @Override
    public List<Attraction> getAttractionsByCategory(Type type) {
        return attractionsByType.getOrDefault(type, Collections.emptyList());
    }

    @Override
    public List<Type> getTypesByCategory(Category category) {
        return typesByCategory.getOrDefault(category, Collections.emptyList());
    }

    @Override
    public List<Category> getAllCategories() {
        return allowedTypes.stream().map(Type::getCategory).distinct().collect(Collectors.toList());
    }

    @Override
    public List<Animal> getShowableAnimalsByType(Type type) {
        return animalsByType.containsKey(type)
            ? animalsByType.get(type)
            .stream()
            .filter(Animal::isShowable)
            .collect(Collectors.toList())
            : Collections.emptyList();
    }
}
