package javari.park.registration;

import java.util.List;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Type;

public class SelectedAttractionImpl implements SelectedAttraction {

    private Attraction attraction;
    private Type type;
    private List<Animal> performers;

    public SelectedAttractionImpl(Attraction attraction, Type type, List<Animal> performers) {
        this.attraction = attraction;
        this.type = type;
        this.performers = performers;
    }

    public Attraction getAttraction() {
        return attraction;
    }

    public Type getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    @Override
    public String toString() {
        return attraction + " -> " + type;
    }
}
