package javari.park.registration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RegistrationImpl implements Registration {

    private static int nextId = 0;
    private int id = nextId++;
    private String visitorName;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<>();

    @Override
    public int getRegistrationId() {
        return id;
    }

    @Override
    public String getVisitorName() {
        return visitorName;
    }

    @Override
    public String setVisitorName(String name) {
        visitorName = name;
        return visitorName;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        return selectedAttractions.add(selected);
    }

    @Override
    public SelectedAttraction cancelLastAttraction() {
        return selectedAttractions.size() == 0
            ? null
            : selectedAttractions.remove(selectedAttractions.size() - 1);
    }

    @Override
    public String toString() {
        return "Name: " + visitorName
            + "\nAttractions: "
            + selectedAttractions.stream()
            .map(Object::toString)
            .collect(Collectors.joining(", "))
            + "\nWith: "
            + selectedAttractions.stream()
            .map(SelectedAttraction::getPerformers)
            .flatMap(List::stream)
            .distinct()
            .map(Object::toString)
            .collect(Collectors.joining(", "));
    }
}
