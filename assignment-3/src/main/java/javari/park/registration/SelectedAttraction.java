package javari.park.registration;

import java.util.List;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Type;

/**
 * This interface describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction that
 * will be watched by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Raihan Ramadistra Pratama
 */
public interface SelectedAttraction {

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    Attraction getAttraction();

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    Type getType();

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    List<Animal> getPerformers();
}
