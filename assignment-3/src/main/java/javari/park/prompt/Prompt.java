package javari.park.prompt;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Prompt is an abstract object to encapsulate user interaction that will return an object of type T
 * after an interaction, given context C.
 *
 * @author Raihan Ramadistra Pratama - 1706027130
 */
public abstract class Prompt<C, T> {

    protected Scanner input;
    protected PrintStream output;
    private List<Prompt> children = new ArrayList<>();

    public Prompt<C, T> setInput(Scanner input) {
        this.input = input;
        for (Prompt p : children) {
            p.setInput(input);
        }
        return this;
    }

    public Prompt<C, T> setOutput(PrintStream output) {
        this.output = output;
        for (Prompt p : children) {
            p.setOutput(output);
        }
        return this;
    }

    protected boolean addChild(Prompt prompt) {
        return children.add(prompt);
    }

    public abstract T prompt(C context);
}