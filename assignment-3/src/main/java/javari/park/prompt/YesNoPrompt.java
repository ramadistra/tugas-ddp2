package javari.park.prompt;

public class YesNoPrompt extends Prompt<String, Boolean> {

    public Boolean prompt(String message) {
        while (true) {
            output.print(message + " (Y/N): ");
            String in = input.nextLine();
            if ("Y".equals(in)) {
                return true;
            } else if ("N".equals(in)) {
                return false;
            }
        }
    }
}
