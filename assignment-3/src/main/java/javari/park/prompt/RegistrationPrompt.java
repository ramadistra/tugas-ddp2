package javari.park.prompt;

import javari.database.ParkDatabase;
import javari.park.registration.Registration;
import javari.park.registration.RegistrationImpl;
import javari.park.registration.SelectedAttraction;

public class RegistrationPrompt extends Prompt<ParkDatabase, Registration> {

    private static String ASK_NAME = "Wow, one more step,\nplease let us know your name";
    private static String ASK_CONTINUE = "\nThank you for your interest. "
        + "Would you like to register to other attractions?";
    private Prompt<ParkDatabase, SelectedAttraction> attractionPrompt;
    private Prompt<String, String> namePrompt;
    private Prompt<String, Boolean> confirmationPrompt;

    public RegistrationPrompt(
        Prompt<ParkDatabase, SelectedAttraction> attractionPrompt,
        Prompt<String, String> namePrompt,
        Prompt<String, Boolean> confirmationPrompt
    ) {
        this.attractionPrompt = attractionPrompt;
        this.namePrompt = namePrompt;
        this.confirmationPrompt = confirmationPrompt;

        addChild(attractionPrompt);
        addChild(namePrompt);
        addChild(confirmationPrompt);
    }

    public Registration prompt(ParkDatabase parkDatabase) {
        Registration registration = new RegistrationImpl();
        while (true) {
            registration.addSelectedAttraction(attractionPrompt.prompt(parkDatabase));

            if (registration.getVisitorName() == null) {
                registration.setVisitorName(namePrompt.prompt(ASK_NAME));
            }

            if (!confirmationPrompt.prompt(confirmData(registration))) {
                registration.cancelLastAttraction();
            } else if (!confirmationPrompt.prompt(ASK_CONTINUE)) {
                return registration;
            }
        }
    }

    private String confirmData(Registration registration) {
        return "\nYeay, final check!\n"
            + "Here is your data, and the attraction you chose:\n"
            + registration
            + "\n\nIs the data correct?";
    }
}
