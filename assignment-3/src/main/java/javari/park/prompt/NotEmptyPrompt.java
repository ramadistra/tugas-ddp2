package javari.park.prompt;

public class NotEmptyPrompt extends Prompt<String, String> {

    public String prompt(String message) {
        output.print("\n" + message + ": ");
        while (true) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                return in;
            }
            output.print("\nInput cannot be empty!\n" + message + ": ");
        }
    }
}
