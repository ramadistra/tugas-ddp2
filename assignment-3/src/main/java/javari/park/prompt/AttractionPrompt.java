package javari.park.prompt;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javari.database.ParkDatabase;
import javari.model.Animal;
import javari.model.Attraction;
import javari.model.Category;
import javari.model.Type;
import javari.park.registration.SelectedAttraction;
import javari.park.registration.SelectedAttractionImpl;

public class AttractionPrompt extends Prompt<ParkDatabase, SelectedAttraction> {

    private static int CATEGORIES = 0;
    private static int TYPES = 1;
    private static int ATTRACTIONS = 2;
    private String parkName;

    public AttractionPrompt(String parkName) {
        this.parkName = parkName;
    }

    @Override
    public SelectedAttraction prompt(ParkDatabase context) {
        String in;
        int index;
        Category category = null;
        Type type = null;
        Attraction attraction = null;
        List<Animal> performingAnimals = Collections.emptyList();

        int state = CATEGORIES;

        output.println("\nPlease answer the questions by typing the number. "
            + "Type # if you want to return to the previous menu");
        while (attraction == null) {
            if (state == ATTRACTIONS && performingAnimals.isEmpty()) {
                state--;
                System.out.println("Unfortunately, no" + type
                    + "can perform any attraction, please choose other animals");
                continue;
            }
            try {
                if (state == CATEGORIES) {
                    showOptionsDialog(parkName + "  has 3 sections:",
                        "section",
                        context.getAllCategories().stream()
                            .map(Category::getSectionName)
                            .collect(Collectors.toList())
                    );
                    in = input.nextLine();
                    index = Integer.parseInt(in) - 1;
                    if (index < context.getAllCategories().size()) {
                        category = (context.getAllCategories()).get(index);
                        state++;
                    }
                } else if (state == TYPES) {
                    showOptionsDialog("--" + category.getSectionName() + "--",
                        "type",
                        context.getTypesByCategory(category)
                    );
                    in = input.nextLine();
                    if ("#".equals(in)) {
                        state--;
                        continue;
                    }
                    index = Integer.parseInt(in) - 1;
                    if (index < context.getTypesByCategory(category).size()) {
                        type = context.getTypesByCategory(category).get(index);
                        performingAnimals = context.getShowableAnimalsByType(type);
                        state++;
                    }
                } else if (state == ATTRACTIONS) {
                    showOptionsDialog("---" + type + "---\nAttractions by " + type + ":",
                        "attraction",
                        context.getAttractionsByCategory(type)
                    );
                    in = input.nextLine();
                    if ("#".equals(in)) {
                        state--;
                        continue;
                    }
                    index = Integer.parseInt(in) - 1;
                    if (index < context.getAttractionsByCategory(type).size()) {
                        attraction = context.getAttractionsByCategory(type).get(index);
                    }
                }
            } catch (NumberFormatException e) {
                // pass
            }
        }

        return new SelectedAttractionImpl(attraction, type, performingAnimals);
    }

    private void showOptionsDialog(String title, String elementName, List<?> options) {
        String dialog = IntStream.rangeClosed(1, options.size())
            .mapToObj(i -> i + ". " + options.get(i - 1))
            .collect(Collectors.joining("\n", "\n" + title + "\n",
                "\nPlease choose your preferred " + elementName + " (type the number): "));

        output.print(dialog);
    }
}
