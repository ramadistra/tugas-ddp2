package javari;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import javari.database.ParkDatabase;
import javari.park.registration.Registration;
import javari.park.prompt.AttractionPrompt;
import javari.park.prompt.NotEmptyPrompt;
import javari.park.prompt.Prompt;
import javari.park.prompt.RegistrationPrompt;
import javari.park.prompt.YesNoPrompt;
import javari.reader.CsvDatabaseReader;
import javari.reader.Reader;
import javari.writer.JsonRegistrationWriter;
import javari.writer.Writer;

public class A3Festival {

    private static String NAME = "Javari Park";
    private static String GREETING = "Welcome to " + NAME + " Festival - Registration Service!";
    private static Scanner input = new Scanner(System.in);
    private static Reader<ParkDatabase> parkDatabaseReader = new CsvDatabaseReader();
    private static Writer<Registration> registrationWriter = new JsonRegistrationWriter();
    private static Prompt<ParkDatabase, Registration> registrationPrompt = new RegistrationPrompt(
        new AttractionPrompt(NAME),
        new NotEmptyPrompt(),
        new YesNoPrompt())
        .setInput(input)
        .setOutput(System.out);

    public static void main(String[] args) {
        Path path = Paths.get("");
        System.out.println(GREETING + "\n\n... Opening default section database from data.");
        while (true) {
            try {
                ParkDatabase parkDatabase = parkDatabaseReader.readFrom(path);
                System.out.println("\n" + GREETING);
                registrationWriter.write(registrationPrompt.prompt(parkDatabase), path);
                return;
            } catch (IOException e) {
                System.out.print("... File not found or incorrect file!\n\n"
                    + "Please provide the source data path: ");
                path = Paths.get(input.nextLine());
            }
        }
    }
}