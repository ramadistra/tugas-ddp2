package javari.model;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Category {
    MAMMAL("Explore the Mammals", animal -> animal.getStatus() != Status.PREGNANT),
    AVES("World of Aves", animal -> animal.getStatus() != Status.LAYING_EGGS),
    REPTILE("Reptilian Kingdom", animal -> animal.getStatus() == Status.TAME);

    private String sectionName;
    private Predicate<Animal> canPeform;
    Category(String sectionName, Predicate<Animal> canPeform) {
        this.sectionName = sectionName;
        this.canPeform = canPeform;
    }

    public static List<String> getSectionNames() {
        return Stream.of(values())
            .map(Category::getSectionName)
            .collect(Collectors.toList());
    }

    public String getSectionName() {
        return sectionName;
    }

    public Predicate<Animal> getCanPerform() {
        return canPeform;
    }
}