package javari.model;

public enum Status {
    NO_STATUS,
    PREGNANT,
    TAME,
    WILD,
    LAYING_EGGS;

    public static Status fromString(String value) {
        return "".equals(value) ? NO_STATUS
            : valueOf(value.toUpperCase().replace(' ', '_'));
    }
}
