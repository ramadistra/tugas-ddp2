package javari.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Attraction {
    CircleOfFire("Circles of Fires", Arrays.asList(Type.LION, Type.WHALE, Type.EAGLE)),
    DancingAnimals("Dancing Animals",
        Arrays.asList(Type.PARROT, Type.SNAKE, Type.CAT, Type.HAMSTER)),
    CountingMasters("Counting Masters", Arrays.asList(Type.HAMSTER, Type.WHALE, Type.PARROT)),
    PassionateCoders("Passionate Coders", Arrays.asList(Type.HAMSTER, Type.CAT, Type.SNAKE));

    private static Map<String, Attraction> mapping;

    static {
        mapping = Stream.of(values())
            .collect(Collectors.toMap(Attraction::getName, Function.identity()));
    }

    private String name;
    private List<Type> allowedTypes;

    Attraction(String name, List<Type> allowedTypes) {
        this.name = name;
        this.allowedTypes = allowedTypes;
    }

    public static Attraction parseAttractionName(String name) {
        if (mapping.containsKey(name)) {
            return mapping.get(name);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static Set<String> getAttractionNames() {
        return mapping.keySet();
    }

    public String getName() {
        return name;
    }

    public boolean isTypeAllowed(Type type) {
        return allowedTypes.contains(type);
    }

    @Override
    public String toString() {
        return name;
    }
}
