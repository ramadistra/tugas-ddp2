package javari.model;

import java.util.function.Predicate;

public enum Type {
    LION(Category.MAMMAL,
        animal -> animal.getGender() == Gender.MALE),
    WHALE(Category.MAMMAL),
    HAMSTER(Category.MAMMAL),
    CAT(Category.MAMMAL),
    PARROT(Category.AVES),
    EAGLE(Category.AVES),
    SNAKE(Category.REPTILE);

    private Category category;
    private Predicate<Animal> canPerform;

    Type(Category category) {
        this.category = category;
        this.canPerform = category.getCanPerform();
    }

    Type(Category category, Predicate<Animal> canPerform) {
        this(category);
        this.canPerform = this.canPerform.and(canPerform);
    }

    public static Type fromString(String name) {
        return valueOf(name.toUpperCase());
    }

    public Predicate<Animal> getCanPerform() {
        return canPerform;
    }

    public Category getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return super.toString().charAt(0) + super.toString().toLowerCase().substring(1);
    }
}
