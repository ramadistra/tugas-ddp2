public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    TrainCar next;
    WildCat cat;
    int count;

    public TrainCar(WildCat cat) {
        this.cat = cat;
        count = 0;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
        count = 1 + (next == null ? 0 : next.count);
    }

    public double getCarWeight() {
        return cat.weight + EMPTY_WEIGHT;
    }

    public double computeTotalWeight() {
        return getCarWeight() + (next == null ? 0 : next.computeTotalWeight());
    }

    public double computeTotalMassIndex() {
        return cat.computeMassIndex() + (next == null ? 0 : next.computeTotalMassIndex());
    }

    public double computeAverageMassIndex() {
        return computeTotalMassIndex() / count;
    }

    @Override
    public String toString() {
        return "(" + cat.name + ")" + (next == null ? "" : next);
    }

    public void printCar() {
        System.out.println("[LOCO]<--" + this);
    }
}
