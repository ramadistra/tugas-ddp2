import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = Integer.parseInt(input.nextLine());
        List<TrainCar> trains = new ArrayList<>();

        TrainCar train = null;
        for (int i = 0; i < n; i++) {
            String[] catArgs = input.nextLine().split(",");
            String name = catArgs[0];
            double weight = Double.parseDouble(catArgs[1]);
            double length = Double.parseDouble(catArgs[2]);
            WildCat cat = new WildCat(name, weight, length);
            train = new TrainCar(cat, train);

            if (train.computeTotalWeight() > THRESHOLD) {
                trains.add(train);
                train = null;
            }
        }

        if (train != null) {
            trains.add(train);
        }

        trains.forEach(A1Station::loadTrain);
    }

    private static void loadTrain(TrainCar train) {
        double massIndex = train.computeAverageMassIndex();

        String category;
        if (massIndex < 18.5) {
            category = "underweight";
        } else if (massIndex < 25) {
            category = "normal";
        } else if (massIndex < 30) {
            category = "overweight";
        } else {
            category = "obese";
        }

        System.out.println("The train departs to Javari Park");
        train.printCar();
        System.out.format("Average mass index of all cats: %.2f\n", massIndex);
        System.out.println("In average, the cats in the train are *" + category + "*");
    }


}
