import animal.Species;
import java.util.Arrays;
import java.util.Scanner;
import park.Park;
import park.arranger.StrangeCageArranger;
import park.display.CageArrangementDisplay;
import park.display.Display;
import park.display.PopulationDisplay;
import park.interaction.Interaction;
import park.interaction.PopulateInteraction;
import park.interaction.VisitInteraction;
import park.store.SimpleAnimalStore;

public class JavariPark extends Park {

    private JavariPark() {
        species = Arrays.asList(Species.values());
        input = new Scanner(System.in).useDelimiter("[,|\n]");
        store = new SimpleAnimalStore();

        setInteraction(Interaction.POPULATE, new PopulateInteraction());
        setInteraction(Interaction.VISIT, new VisitInteraction(), Arrays.asList(
            Species.Cat,
            Species.Eagle,
            Species.Hamster,
            Species.Parrot,
            Species.Lion
        ));
        setDisplay(Display.POPULATION, new PopulationDisplay());
        setDisplay(Display.CAGES, new CageArrangementDisplay(new StrangeCageArranger()));
    }

    public static void main(String[] args) {
        Park park = new JavariPark();
        System.out.println("Welcome to Javari Park!");
        park.interact(Interaction.POPULATE);
        park.display(Display.CAGES);
        park.display(Display.POPULATION);
        park.interact(Interaction.VISIT);
    }
}