package park;

import animal.Species;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import park.display.Display;
import park.interaction.Interaction;
import park.store.AnimalStore;

public abstract class Park {

    protected AnimalStore store;
    protected List<Species> species;
    protected Scanner input;
    private Map<String, Display> displays = new HashMap<>();
    private Map<String, Interaction> interactions = new HashMap<>();

    public void setInteraction(String name, Interaction interaction) {
        setInteraction(name, interaction, species);
    }

    public void setDisplay(String type, Display display) {
        setDisplay(type, display, species);
    }

    public void setInteraction(String name, Interaction interaction, List<Species> speciesOrder) {
        interactions.put(name, interaction);
        interaction.setSpeciesOrder(speciesOrder);
        interaction.setInput(input);
        interaction.setStore(store);
    }

    public void setDisplay(String type, Display display, List<Species> speciesOrder) {
        displays.put(type, display);
        display.setStore(store);
        display.setSpeciesOrder(speciesOrder);
    }

    public void interact(String type) {
        if (!interactions.containsKey(type)) {
            System.out.println("No such interaction");
        } else {
            interactions.get(type).interact();
        }
    }

    public void display(String name) {
        if (!displays.containsKey(name)) {
            System.out.println("No such display");
        } else {
            displays.get(name).display();
        }
    }
}
