package park.display;

import animal.Species;
import cage.Cage;
import java.util.List;
import park.arranger.Arranger;

public class CageArrangementDisplay extends Display {

    private Arranger<Cage> arranger;

    public CageArrangementDisplay(Arranger<Cage> arranger) {
        this.arranger = arranger;
    }

    @Override
    public void display() {
        StringBuilder arrangementString = new StringBuilder("\nCage arrangement:");

        speciesOrder.stream()
            .filter(s -> !store.getCagesOfSpecies(s).isEmpty())
            .map(this::getArrangements)
            .forEach(arrangementString::append);

        System.out.println(arrangementString);
    }

    private String getArrangements(Species species) {
        return "\nlocation: "
            + (species.isWild() ? "outdoor" : "indoor")
            + formatArrangment(arranger.arrange(store.getCagesOfSpecies(species)))
            + "\n\nAfter rearrangement..."
            + formatArrangment(arranger.rearrange(store.getCagesOfSpecies(species)))
            + "\n";
    }

    private String formatArrangment(List<List<Cage>> arrangement) {
        StringBuilder sb = new StringBuilder();
        for (int i = arrangement.size(); i > 0; i--) {
            sb.append("\nlevel ").append(i).append(": ");
            for (Cage cage : arrangement.get(i - 1)) {
                sb.append(cage).append(", ");
            }
        }

        return sb.toString();
    }
}
