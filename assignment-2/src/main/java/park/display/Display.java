package park.display;

import animal.Species;
import java.util.List;
import park.store.AnimalStore;

public abstract class Display {

    public final static String CAGES = "cages";
    public final static String POPULATION = "population";

    AnimalStore store;
    List<Species> speciesOrder;

    Display() {
    }

    public void setSpeciesOrder(List<Species> speciesOrder) {
        this.speciesOrder = speciesOrder;

    }

    public void setStore(AnimalStore store) {
        this.store = store;
    }

    public abstract void display();
}
