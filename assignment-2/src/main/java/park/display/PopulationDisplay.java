package park.display;

import animal.Species;

public class PopulationDisplay extends Display {

    @Override
    public void display() {
        StringBuilder populationInfo = new StringBuilder("NUMBER OF ANIMALS:");
        for (Species species : speciesOrder) {
            populationInfo.append("\n")
                .append(species.name().toLowerCase())
                .append(":")
                .append(store.getCagesOfSpecies(species).size());
        }
        System.out.println(populationInfo);
    }
}
