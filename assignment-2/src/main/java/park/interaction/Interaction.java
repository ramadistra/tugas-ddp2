package park.interaction;

import animal.Species;
import java.util.List;
import java.util.Scanner;
import park.store.AnimalStore;

public abstract class Interaction {

    public final static String VISIT = "visit";
    public final static String POPULATE = "populate";
    Scanner input;
    AnimalStore store;
    List<Species> speciesOrder;

    Interaction() {
    }

    public void setSpeciesOrder(List<Species> speciesOrder) {
        this.speciesOrder = speciesOrder;
    }

    public void setInput(Scanner newInput) {
        input = newInput;
    }

    public void setStore(AnimalStore newStore) {
        store = newStore;
    }

    public abstract void interact();
}
