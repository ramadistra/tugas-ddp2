package park.interaction;

import action.Action;
import animal.Animal;
import animal.Species;
import java.util.List;

;


public class VisitInteraction extends Interaction {

    private String getMenu() {
        StringBuilder sb = new StringBuilder("Which animal would you like to visit\n(");
        for (int i = 0; i < speciesOrder.size(); i++) {
            sb.append(i + 1)
                .append(":")
                .append(" ")
                .append(speciesOrder.get(i).name())
                .append(", ");
        }
        sb.append("99:exit)");

        return sb.toString();
    }

    public void interact() {
        System.out.println("\n=============================================");
        while (true) {
            System.out.println(getMenu());

            int code = input.nextInt();
            if (code == 99) {
                break;
            }
            Species species = speciesOrder.get(code - 1);

            System.out.println("Mention the name of "
                + species.name().toLowerCase()
                + " you want to visit: ");

            input.nextLine();
            String name = input.nextLine();

            Animal animal = store.getAnimalBySpeciesAndName(species, name);

            if (animal == null) {
                System.out
                    .println("There is no " + species.name().toLowerCase() + " with that name!");
                continue;
            }

            StringBuilder prompt = new StringBuilder("You are visiting ");
            prompt.append(animal.getName())
                .append(" (")
                .append(species.name().toLowerCase())
                .append(") ")
                .append("what would you like to do?\n");

            List<Action> actions = animal.getSpecies().getActions();
            for (int i = 0; i < actions.size(); i++) {
                prompt.append(i + 1).append(": ").append(actions.get(i)).append(" ");
            }

            System.out.println(prompt);
            int actionCode = Integer.parseInt(input.nextLine());
            System.out.println(animal.executeAction(actionCode) + "\nBack to the office!");
        }
    }
}
