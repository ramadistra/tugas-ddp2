package park.interaction;

import animal.Species;

public class PopulateInteraction extends Interaction {

    @Override
    public void interact() {
        System.out.println("Input the number of animals");
        for (Species s : speciesOrder) {
            String speciesName = s.name().toLowerCase();
            System.out.print(speciesName + ": ");
            int count = Integer.parseInt(input.nextLine());
            if (count <= 0) {
                continue;
            }
            System.out.println("Provide the information of " + speciesName + "(s):");

            for (int i = 0; i < count; i++) {
                String name = input.next();
                int size = input.nextInt();
                store.addAnimal(s.createAnimal(name, size));
            }
            input.nextLine();
        }
    }
}
