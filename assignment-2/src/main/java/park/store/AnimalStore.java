package park.store;

import animal.Animal;
import animal.Species;
import cage.Cage;
import java.util.List;

public interface AnimalStore {

    void addAnimal(Animal animal);

    List<Cage> getCagesOfSpecies(Species species);

    Animal getAnimalBySpeciesAndName(Species species, String name);
}
