package park.store;

import animal.Animal;
import animal.Species;
import cage.Cage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleAnimalStore implements AnimalStore {

    private Map<Species, List<Cage>> speciesCages;
    private Map<Species, Map<String, Animal>> animalsBySpecies;

    public SimpleAnimalStore() {
        speciesCages = new HashMap<>();
        animalsBySpecies = new HashMap<>();
    }

    public void addAnimal(Animal animal) {
        createNewEntryIfEmpty(animal.getSpecies());
        speciesCages.get(animal.getSpecies()).add(new Cage(animal));
        animalsBySpecies.get(animal.getSpecies()).put(animal.getName(), animal);
    }

    public List<Cage> getCagesOfSpecies(Species species) {
        createNewEntryIfEmpty(species);
        return speciesCages.get(species);
    }

    public Animal getAnimalBySpeciesAndName(Species species, String name) {
        createNewEntryIfEmpty(species);
        return animalsBySpecies.get(species).get(name);
    }

    private void createNewEntryIfEmpty(Species species) {
        if (!speciesCages.containsKey(species)) {
            speciesCages.put(species, new ArrayList<>());
        }
        if (!animalsBySpecies.containsKey(species)) {
            animalsBySpecies.put(species, new HashMap<>());
        }
    }
}
