package park.arranger;

import java.util.List;

public interface Arranger<T> {

    List<List<T>> arrange(List<T> items);

    List<List<T>> rearrange(List<T> items);
}
