package park.arranger;

import cage.Cage;
import java.util.Arrays;
import java.util.List;

public class StrangeCageArranger implements Arranger<Cage> {

    public List<List<Cage>> arrange(List<Cage> cages) {
        int levelSize = cages.size() / 3 + (cages.size() % 3) / 2;

        return Arrays.asList(
            cages.subList(0, levelSize),
            cages.subList(levelSize, levelSize * 2),
            cages.subList(levelSize * 2, cages.size())
        );
    }

    public List<List<Cage>> rearrange(List<Cage> cages) {
        int levelSize = cages.size() / 3 + (cages.size() % 3) / 2;

        List<List<Cage>> arrangement = Arrays.asList(
            cages.subList(levelSize * 2, cages.size()),
            cages.subList(0, levelSize),
            cages.subList(levelSize, levelSize * 2)
        );
        arrangement.forEach(this::reverse);

        return arrangement;
    }

    private void reverse(List<Cage> cages) {
        for (int i = 0; i < cages.size() / 2; i++) {
            Cage tmp = cages.get(i);
            cages.set(i, cages.get(cages.size() - i - 1));
            cages.set(cages.size() - i - 1, tmp);
        }
    }
}
