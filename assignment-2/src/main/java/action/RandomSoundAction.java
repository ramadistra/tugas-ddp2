package action;

import java.text.Format;
import java.text.MessageFormat;
import java.util.Random;

public class RandomSoundAction extends Action {

    private Format[] templates;

    public RandomSoundAction(String name, String[] sounds) {
        super(name);

        this.templates = new MessageFormat[sounds.length];
        for (int i = 0; i < templates.length; i++) {
            this.templates[i] = new MessageFormat(
                String.format("{0} makes a voice: %s", sounds[i]));
        }
    }

    @Override
    public Format getTemplate() {
        Random ran = new Random();
        int x = ran.nextInt(4);
        return templates[x];
    }
}
