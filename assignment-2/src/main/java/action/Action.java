package action;


import java.text.Format;
import java.text.MessageFormat;

public class Action {

    private String name;
    private MessageFormat template;

    public Action(String name, String template) {
        this.name = name;
        this.template = new MessageFormat(template);
    }

    Action(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    protected Format getTemplate() {
        return template;
    }

    public String execute(String actorName) {
        return getTemplate().format(new Object[]{actorName});
    }

    ;
}
