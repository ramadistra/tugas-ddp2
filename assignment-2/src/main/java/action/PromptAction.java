package action;


import java.util.Scanner;

public class PromptAction extends Action {

    private String[] prompts;

    public PromptAction(String name, String template, String[] prompts) {
        super(name, template);
        this.prompts = prompts;
    }

    @Override
    public String execute(String actorName) {
        Object[] templateArgs = new String[prompts.length];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < prompts.length; i++) {
            System.out.print(prompts[i]);
            templateArgs[i] = scanner.nextLine();
        }

        return String.format(super.execute(actorName), templateArgs);
    }
}
