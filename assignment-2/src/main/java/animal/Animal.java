package animal;

public class Animal {

    private Species species;
    private String name;
    private int size;

    Animal(Species species, String name, int size) {
        this.species = species;
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public Species getSpecies() {
        return species;
    }

    public String executeAction(int code) {
        return species.getActions().get(code - 1).execute(name);
    }
}