package animal;

import action.Action;
import action.PromptAction;
import action.RandomSoundAction;
import java.util.Arrays;
import java.util.List;

public enum Species {
    Cat(false, Arrays.asList(
        new Action("Brush the fur",
            "Time to clean {0}''s fur\n{0} makes a voice: Nyaaan..."),
        new RandomSoundAction("Cuddle",
            new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"})
    )),
    Lion(true, Arrays.asList(
        new Action("See it hunting",
            "Lion is hunting..\n{0} makes a voice: err...!"),
        new Action("Brush the mane",
            "Clean the lion’s mane..\n{0} makes a voice: Hauhhmm!"),
        new Action("Disturb it", "Simba makes a voice: HAUHHMM!")
    )),
    Eagle(true, Arrays.asList(
        new Action("Order to fly",
            "{0} makes a voice: kwaakk…\nYou hurt!"))),
    Parrot(false, Arrays.asList(
        new Action("Order to fly",
            "Parrot {0} flies!\n{0} makes a voice: FLYYYY…..\n"),
        new PromptAction("Do conversation",
            "{0} says: %S..", new String[]{"You say: "})
    )),
    Hamster(false, Arrays.asList(
        new Action("See it gnawing",
            "{0} makes a voice: ngkkrit.. ngkkrrriiit"),
        new Action("Order to run in the hamster wheel",
            "{0} makes a voice: trrr…. trrr...")
    ));


    private boolean isWild;
    private List<Action> actions;

    Species(boolean isWild, List<Action> actions) {
        this.isWild = isWild;
        this.actions = actions;
    }

    public Animal createAnimal(String name, int size) {
        return new Animal(this, name, size);
    }

    public boolean isWild() {
        return isWild;
    }

    public List<Action> getActions() {
        return actions;
    }
}
