package cage;

import animal.Animal;

public class Cage {

    private Animal animal;

    public Cage(Animal animal) {
        this.animal = animal;
    }

    @Override
    public String toString() {
        return animal.getName() + " (" + animal.getSize() + " - " + getCageType() + ")";
    }

    public String getCageType() {
        int size = animal.getSize();

        if (animal.getSpecies().isWild()) {
            if (size < 45) {
                return "A";
            } else if (size < 60) {
                return "B";
            } else {
                return "C";
            }
        } else {
            if (size < 75) {
                return "A";
            } else if (size <= 90) {
                return "B";
            } else {
                return "C";
            }
        }
    }
}