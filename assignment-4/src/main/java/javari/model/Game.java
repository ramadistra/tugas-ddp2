package javari.model;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.stream.Stream;

public class Game {
    private SwingPropertyChangeSupport propChange;
    private Board board;
    private Long startTime;
    private boolean isStarted;
    private int tries;
    private boolean isWon;

    public Game(Board board) {
        propChange = new SwingPropertyChangeSupport(this);
        this.board = board;
        this.isStarted = false;
        this.startTime = null;
        this.isWon = false;
    }

    public void addListener(PropertyChangeListener prop) {
        propChange.addPropertyChangeListener(prop);
    }

    public Board getBoard() {
        return board;
    }

    public int getTries() {
        return tries;
    }

    private void setTries(int tries) {
        int oldTries = this.tries;
        this.tries = tries;
        propChange.firePropertyChange("tries", oldTries, this.tries);
    }

    public boolean canOpenCard() {
        return getOpenedCards().count() < board.getMatchNumber();
    }

    public boolean open(Card card) {
        if (canOpenCard()) {
            card.setState(Card.State.OPENED);
            return true;
        } else {
            return false;
        }
    }

    public void matchOpenCards() {
        boolean allCardsMatch = getOpenedCards().findAny()
                .map(c -> getOpenedCards().allMatch(c::match))
                .orElse(false);

        getOpenedCards().forEach(card ->
                card.setState(allCardsMatch ? Card.State.DISCARDED : Card.State.CLOSED));

        setTries(tries + 1);
        updateWinStatus();
    }

    public void start() {
        setIsStarted(true);
        board.setCardsState(Card.State.CLOSED);
        startTime = System.currentTimeMillis();
    }

    private void setIsStarted(boolean isStarted) {
        boolean prev = this.isStarted;
        this.isStarted = isStarted;
        propChange.firePropertyChange("is_started", prev, this.isStarted);
    }

    public boolean isStarted() {
        return isStarted;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void reset() {
        startTime = null;
        setIsStarted(false);
        board.setCardsState(Card.State.CLOSED);
        board.shuffle();
        setTries(0);
    }

    private void updateWinStatus() {
        boolean prev = isWon;
        isWon = board.getCards().stream().allMatch(c -> c.getState() == Card.State.DISCARDED);
        propChange.firePropertyChange("is_won", prev, isWon);
    }

    private Stream<Card> getOpenedCards() {
        return board.getCards().stream().filter(c -> c.getState() == Card.State.OPENED);
    }
}
