package javari.model;

import javax.swing.Icon;
import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Board implements Iterable<Card> {
    private SwingPropertyChangeSupport propChange;
    private int matchNumber;
    private List<Card> cards;

    public Board(int matchNumber, Icon cardCover, List<Icon> cardFaces) {
        this.propChange = new SwingPropertyChangeSupport(this);
        this.matchNumber = matchNumber;
        cards = cardFaces.stream()
                .flatMap(c -> IntStream.range(0, matchNumber).mapToObj(i -> new Card(cardCover, c)))
                .collect(Collectors.toList());
        shuffle();
    }

    public Board(Icon cardCover, List<Icon> cardFaces) {
        this(2, cardCover, cardFaces);
    }

    public int getMatchNumber() {
        return matchNumber;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void shuffle() {
        List<Card> newShuffle = new ArrayList<>(cards);
        Collections.shuffle(newShuffle);
        propChange.firePropertyChange("arrangement", this.cards, this.cards = newShuffle);
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    public void setCardsState(Card.State state) {
        forEach(c -> c.setState(state));
    }

    public void addListener(PropertyChangeListener listener) {
        propChange.addPropertyChangeListener(listener);
    }
}
