package javari.model;

import javax.swing.Icon;
import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;

public class Card {
    private SwingPropertyChangeSupport propChange;
    private State state;
    private Icon cover;
    private Icon face;

    public Card(Icon cover, Icon cardData) {
        this.propChange = new SwingPropertyChangeSupport(this);
        this.state = State.CLOSED;
        this.cover = cover;
        this.face = cardData;
    }

    public void addListener(PropertyChangeListener prop) {
        propChange.addPropertyChangeListener(prop);
    }

    public State getState() {
        return state;
    }

    public void setState(State nextState) {
        State prevState = state;
        state = nextState;
        propChange.firePropertyChange("state", prevState, nextState);
    }

    public Icon getFace() {
        return face;
    }

    public Icon getCover() {
        return cover;
    }

    public boolean match(Card other) {
        return other.face == face;
    }

    public enum State {
        OPENED,
        CLOSED,
        DISCARDED
    }
}
