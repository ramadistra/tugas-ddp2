package javari.controller;

import javari.model.Card;
import javari.model.Game;
import javari.util.ActionUtil;
import javari.view.GameView;

/**
 * Game controller interprets the user's action and modifies the state of the game accordingly.
 *
 * @author ramadistra
 */
public class GameController implements BoardController {
    private Game model;
    private GameView view;

    public GameController(Game game) {
        this.model = game;
        this.view = new GameView(model, this);
        view.createView();
    }

    public void start() {
        view.getStartButton().setEnabled(false);
        view.getPlayAgainButton().setEnabled(false);
        model.getBoard().setCardsState(Card.State.OPENED);

        ActionUtil.delayedAction(1150, e -> {
            model.start();
            view.getPlayAgainButton().setEnabled(true);
        });
    }

    public void select(Card card) {
        if (!model.isStarted()) {
            start();
            return;
        }
        if (model.open(card) && !model.canOpenCard()) {
            model.matchOpenCards();
        }
    }

    public void resetGame() {
        view.getStartButton().setEnabled(true);
        model.reset();
    }

    public void exit() {
        view.getComponent().dispose();
    }
}
