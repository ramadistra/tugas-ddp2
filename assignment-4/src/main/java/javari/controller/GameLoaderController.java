package javari.controller;

import javari.model.Board;
import javari.model.Game;
import javari.util.AssetsLoader;
import javari.view.AssetsNotFoundView;
import javari.view.PathChooserView;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * GameLoaderController initializes the game and displays an error message when the game cannot be
 * properly loaded
 *
 * @author ramadistra
 */
public class GameLoaderController implements PathChooserController {
    private static String DEFAULT_PATH = "assets";
    private AssetsNotFoundView notFoundView;
    private PathChooserView folderChooserView;

    public GameLoaderController() {
        folderChooserView = new PathChooserView(this);
        folderChooserView.createView();
        notFoundView = new AssetsNotFoundView(this);

        loadGameFromPath(Paths.get(DEFAULT_PATH));
    }

    public void openChooser() {
        notFoundView.getComponent().setVisible(false);
        folderChooserView.getComponent().setVisible(true);
    }

    public void pathNotChosen() {
        folderChooserView.getComponent().setVisible(false);
        notFoundView.showDialog("Please specify assets folder");
    }

    public void pathChosen(Path path) {
        folderChooserView.getComponent().setVisible(false);
        loadGameFromPath(path);
    }

    private void loadGameFromPath(Path path) {
        try {
            AssetsLoader loader = new AssetsLoader(path);
            new GameController(new Game(
                    new Board(loader.loadCardCover(), loader.loadCardFaces())
            ));
            notFoundView.getComponent().dispose();
            folderChooserView.getComponent().dispose();
        } catch (IOException e) {
            notFoundView.showDialog("Game assets could not be loaded.");
        }
    }
}
