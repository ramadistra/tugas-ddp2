package javari.controller;

import java.nio.file.Path;

/**
 * PathChooserController is a controller to manage the behavior of prompting the user for a path.
 */
public interface PathChooserController {
    /**
     * Invoked when the user has chosen a path.
     *
     * @param path the path that the user has chosen.
     */
    void pathChosen(Path path);

    /**
     * Invoked when no path was chosen by the user.
     */
    void pathNotChosen();
}
