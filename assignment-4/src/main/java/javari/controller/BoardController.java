package javari.controller;

import javari.model.Card;

/**
 * BoardController is the controller for the Board model.
 * Mainly used to delegate the behavior after a card has been selected.
 *
 * @author ramadistra
 */
public interface BoardController {
    void select(Card card);
}
