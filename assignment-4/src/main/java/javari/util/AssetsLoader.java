package javari.util;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class to encapsulate loading the game's assets.
 */
public class AssetsLoader {
    private int cardWidth;
    private int cardHeight;
    private Path assetsPath;

    public AssetsLoader(Path assetsPath) {
        this(assetsPath, 96, 96);
    }

    public AssetsLoader(Path assetsPath, int cardWidth, int cardHeight) {
        this.cardWidth = cardWidth;
        this.cardHeight = cardHeight;
        this.assetsPath = assetsPath;
    }

    public List<Icon> loadCardFaces() throws IOException {
        List<Icon> icons = Files.walk(assetsPath.resolve("faces"))
                .filter(Files::isRegularFile)
                .map(Object::toString)
                .filter(s -> s.matches(".*\\.(png|jpg)$"))
                .map(this::getScaledIcon)
                .collect(Collectors.toList());

        if (icons.isEmpty()) {
            throw new IOException("No images in faces folder found");
        } else {
            return icons;
        }
    }

    public Icon loadCardCover() {
        return getScaledIcon(assetsPath.resolve("cover.jpg").toString());
    }

    private ImageIcon getScaledIcon(String filename) {
        return new ImageIcon(new ImageIcon(filename)
                .getImage()
                .getScaledInstance(cardWidth, cardHeight, Image.SCALE_SMOOTH));
    }
}
