package javari.util;

import javax.swing.Timer;
import java.awt.event.ActionListener;

/**
 * Utility to simplify creating delayed actions using Swing timers.
 */
public class ActionUtil {
    public static void delayedAction(int delay, ActionListener listener) {
        Timer timer = new Timer(delay, listener);
        timer.setRepeats(false);
        timer.start();
    }
}
