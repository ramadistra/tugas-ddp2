package javari;

import javari.controller.GameLoaderController;

import javax.swing.SwingUtilities;

public class A4Game {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(GameLoaderController::new);
    }
}
