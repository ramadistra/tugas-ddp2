package javari.view;

import javari.model.Card;
import javari.util.ActionUtil;

import javax.swing.JButton;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CardView implements View<JButton>, PropertyChangeListener {
    private Card model;
    private Runnable controller;
    private JButton button;

    public CardView(Card model, Runnable controller) {
        this.model = model;
        this.button = new JButton();
        this.controller = controller;

        this.model.addListener(this);
        this.button.setIcon(this.model.getCover());
        this.button.setDisabledIcon(this.model.getFace());
        this.button.addActionListener(e -> this.controller.run());
        displayCardState(model.getState());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equalsIgnoreCase(evt.getPropertyName())) {
            handleStateChange((Card.State) evt.getOldValue(), (Card.State) evt.getNewValue());
        }
    }

    @Override
    public JButton getComponent() {
        return button;
    }

    private void handleStateChange(Card.State prevState, Card.State nextState) {
        if (prevState == Card.State.OPENED) {
            ActionUtil.delayedAction(250, e -> displayCardState(nextState));
        } else {
            displayCardState(nextState);
        }
    }

    private void displayCardState(Card.State state) {
        switch (state) {
            case OPENED:
                button.setVisible(true);
                button.setEnabled(false);
                break;
            case CLOSED:
                button.setVisible(true);
                button.setEnabled(true);
                break;
            case DISCARDED:
                button.setVisible(false);
                break;
        }
    }
}
