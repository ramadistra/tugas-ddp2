package javari.view;

import java.awt.Component;

/**
 * Utility interface to compose UI components instead of using inheritance.
 *
 * @author Raihan Ramadistra Pratama - 1706027130
 */
public interface View<T extends Component> {
    T getComponent();
}
