package javari.view;

import javari.controller.PathChooserController;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PathChooserView implements View<JFrame>, ActionListener {
    private PathChooserController controller;
    private JFrame frame;
    private JFileChooser fileChooser;

    public PathChooserView(PathChooserController controller) {
        this.controller = controller;
    }

    public void createView() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                controller.pathNotChosen();
            }
        });

        fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.addActionListener(this);

        frame.add(fileChooser);
        frame.pack();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(JFileChooser.APPROVE_SELECTION)) {
            controller.pathChosen(fileChooser.getSelectedFile().toPath());
        } else if (e.getActionCommand().equals(JFileChooser.CANCEL_SELECTION)) {
            controller.pathNotChosen();
        }
    }

    @Override
    public JFrame getComponent() {
        return frame;
    }
}
