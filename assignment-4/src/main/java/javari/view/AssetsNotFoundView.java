package javari.view;

import javari.controller.GameLoaderController;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class AssetsNotFoundView implements View<JFrame> {
    private GameLoaderController controller;
    private JFrame frame;

    public AssetsNotFoundView(GameLoaderController controller) {
        this.controller = controller;
        frame = new JFrame();
    }

    public void showDialog(String message) {
        String[] options = {"Choose assets directory", "Exit"};

        int n = JOptionPane.showOptionDialog(frame,
                message,
                "Fatal error",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);

        if (n == 1) {
            System.exit(0);
        } else {
            controller.openChooser();
        }
    }

    @Override
    public JFrame getComponent() {
        return frame;
    }
}
