package javari.view;

import javari.controller.GameController;
import javari.model.Game;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class GameView implements View<JFrame>, PropertyChangeListener, ActionListener {
    private static String TIME_ELAPSED = "Time elapsed: ";
    private static String NULL_DURATION = TIME_ELAPSED + "--";

    private Game model;
    private GameController controller;
    private Timer durationTimer;
    private JFrame frame;
    private JPanel panel;
    private JPanel cards;
    private JPanel menu;
    private JLabel duration;
    private JLabel tries;
    private JButton start;
    private JButton playAgain;
    private JButton exit;

    public GameView(Game model, GameController controller) {
        this.model = model;
        this.controller = controller;
        this.model.addListener(this);
    }

    public void createView() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new JPanel();
        LayoutManager layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);
        frame.add(panel);

        cards = new BoardView(model.getBoard(), controller).getComponent();
        panel.add(cards);

        menu = new JPanel();
        panel.add(menu);

        tries = new JLabel(getTryText());
        panel.add(tries);

        duration = new JLabel(NULL_DURATION);
        durationTimer = new Timer(100, this);
        durationTimer.setInitialDelay(0);
        panel.add(duration);

        playAgain = new JButton("Restart Game");
        playAgain.addActionListener(this);
        menu.add(playAgain);

        start = new JButton("Start");
        start.addActionListener(this);
        menu.add(start);

        exit = new JButton("Exit");
        exit.addActionListener(this);
        menu.add(exit);

        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public JFrame getComponent() {
        return frame;
    }

    public JButton getPlayAgainButton() {
        return playAgain;
    }

    public JButton getStartButton() {
        return start;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("tries".equalsIgnoreCase(evt.getPropertyName())) {
            updateTries();
        } else if ("is_started".equalsIgnoreCase(evt.getPropertyName())) {
            handleGameStatusChange((boolean) evt.getNewValue());
        } else if ("is_won".equalsIgnoreCase(evt.getPropertyName())) {
            handleWinStatusChange((boolean) evt.getOldValue(), (boolean) evt.getNewValue());
        }
    }

    private void handleGameStatusChange(boolean isStarted) {
        if (isStarted) {
            durationTimer.start();
        } else {
            durationTimer.stop();
            displayDuration();
        }
    }

    private void handleWinStatusChange(boolean prev, boolean next) {
        if (!prev && next) {
            durationTimer.stop();
            displayMessage("You win!");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == playAgain) {
            controller.resetGame();
        } else if (e.getSource() == exit) {
            controller.exit();
        } else if (e.getSource() == start) {
            controller.start();
        } else if (e.getSource() == durationTimer) {
            displayDuration();
        }
    }

    private void displayDuration() {
        if (model.getStartTime() == null) {
            duration.setText(NULL_DURATION);
        } else if (model.isStarted()) {
            duration.setText(TIME_ELAPSED
                    + (System.currentTimeMillis() - model.getStartTime()) / 1000
                    + " seconds");
        }
    }

    public void displayMessage(String message) {
        JOptionPane.showMessageDialog(frame, message);
    }

    private void updateTries() {
        tries.setText(getTryText());
    }

    private String getTryText() {
        return "Tries: " + model.getTries();
    }
}
