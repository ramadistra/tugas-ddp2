package javari.view;

import javari.controller.BoardController;
import javari.model.Board;

import javax.swing.JPanel;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class BoardView implements View<JPanel>, PropertyChangeListener {
    private Board model;
    private BoardController controller;
    private JPanel board;

    public BoardView(Board model, BoardController controller) {
        this.model = model;
        this.controller = controller;
        model.addListener(this);
        board = new JPanel(new GridLayout(gridSize(), gridSize()));
        renderCards();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equalsIgnoreCase("arrangement")) {
            renderCards();
        }
    }

    @Override
    public JPanel getComponent() {
        return board;
    }

    private void renderCards() {
        board.removeAll();
        model.getCards().stream()
                .map(card -> new CardView(card, () -> controller.select(card)))
                .map(CardView::getComponent)
                .forEach(board::add);
        board.revalidate();
        board.repaint();
    }

    private int gridSize() {
        return (int) Math.ceil(Math.sqrt(model.getCards().size()));
    }
}
